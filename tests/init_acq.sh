#!/bin/sh
#

. $HOME/bde/tools/src/dev.env

caput LAB-090Row:PBI-AMC-011:NumSamples 131072 || exit 1
caput LAB-090Row:PBI-AMC-011:ClockSource 6 || exit 1
caput LAB-090Row:PBI-AMC-011:ClockDiv 1 || exit 1
caput LAB-090Row:PBI-AMC-011:SamplingFreq 88052500 || exit 1
caput LAB-090Row:PBI-AMC-011:RTMType 4 || exit 1

caput LAB-090Row:PBI-AMC-011:CHControlAll 1 || exit 1
# caput LAB-090Row:PBI-AMC-011:I1-CHControl 1 || exit 1
# caput LAB-090Row:PBI-AMC-011:I2-CHControl 1 || exit 1
# caput LAB-090Row:PBI-AMC-011:RF-M-Control 1 || exit 1
# caput LAB-090Row:PBI-AMC-011:RF-A-Control 1 || exit 1

caput LAB-090Row:PBI-AMC-011:SelfTrigControl 1 || exit 1
caput LAB-090Row:PBI-AMC-011:SelfTrigThreshold 10 || exit 1
caput LAB-090Row:PBI-AMC-011:SelfTrigIQSamples 50000 || exit 1
caput LAB-090Row:PBI-AMC-011:SelfTrigChRef 1 || exit 1

exit 0
