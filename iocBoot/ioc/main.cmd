###############################################################################
# Top script that includes other sub unit scripts.

# get paths to EPICS base and modules
< envPaths

errlogInit(20000)

dbLoadDatabase("$(BPMAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

# PATH: we need caput and caRepeater
epicsEnvSet("PATH",                         "$(PATH):$(EPICS_BASE)/bin/linux-x86_64")
# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
# EPICS_DB_INCLUDE_PATH: list all module db folders
epicsEnvSet("EPICS_DB_INCLUDE_PATH",        "$(TOP)/iocBoot:$(AUTOSAVE)/db:$(ADCORE)/db:$(ADSIS8300)/db:$(ADMISC)/db:$(ADSIS8300BPM)/db:$(BPMAPP)/db:$(MRFIOC2)/db:$(ETHMOD)/db")

# digitizer support
< dataAcquisition.cmd

# timing support
< timing.cmd

# ethmod support
# < ethmod.cmd

###############################################################################

iocInit()

###############################################################################

# save things every thirty seconds
# create_monitor_set("auto_settings.req", 30, "P=$(ACQ_PREFIX)")

###############################################################################

< post_init.cmd

#######################################
#     End of IOC startup commands     #
#######################################
