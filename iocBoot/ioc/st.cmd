########################################
#        IOC startup entry point       #
########################################

# instance.cmd defines all variables that differe between instances of this
# IOC. For example hardware IDs, Naming service names,..
< instance.cmd

# main.cmd will start with loading loading system components, call
# iocBoot() and run all the dbpf commands after IOC has started.
< main.cmd
